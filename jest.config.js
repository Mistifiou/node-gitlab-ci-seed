module.exports = {
    clearMocks: true,
    collectCoverage: true,
    coverageDirectory: 'coverage',
    coverageReporters: ['json', 'text', 'lcov', 'clover'],
    modulePathIgnorePatterns: ['assets', 'test/assets'],
    setupFilesAfterEnv: ['./jest.setup.js'],
    testEnvironment: 'node',
    testMatch: ['**/src/tests/**/*.test.js']
};
