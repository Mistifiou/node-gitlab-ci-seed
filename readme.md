# Welcome to NodeJS Gitlab-ci seed documentation

This project have objective to be a proof of concept for CI system with gitlab.

![node](https://img.shields.io/node/v/latest)

## Installation

```bash
git clone gitlab.com/Mistifiou/gitlab-ci-seed.git ./my-project
cd ./my-project
rm -rf ./.git
git remote add origin MY_GITLAB_ORIGIN
git init && git add . && git commit -m "from gitlab.com/Mistifiou/gitlab-ci-seed" && git push -u HEAD
```

### Gitlab configuration

Create a sample project or configure it after your first push.

#### env vars

- **CI_PUSH_TOKEN:** Gitlab CI token to generate with your ci account
- **NPM_AUTH_TOKEN:** NPM auth token to publish project on npm
- **HUSKY_SKIP_HOOKS:** Will tell to Husky to not validate project in ci ENV. Must be `1`

### Protected branches and tags

Protect following branches by wildcard: `dev`, `release/*`  
Protect all tags by wildcard: `*`

## Project issues, questions and remarks

[Add issue](https://gitlab.com/Mistifiou/gitlab-ci-seed/issues)

# Documentation

Documentation will come soon with gitlab-pages [here](https://Mistifiou.gitlab.io/gitlab-ci-seed)  

# CI

[![pipeline status](https://gitlab.com/Mistifiou/gitlab-ci-seed/badges/master/pipeline.svg)](https://gitlab.com/Mistifiou/gitlab-ci-seed/commits/master)
[![coverage](https://gitlab.com/Mistifiou/gitlab-ci-seed/badges/master/coverage.svg)](https://gitlab.com/Mistifiou/gitlab-ci-seed/badges/master/coverage.svg)

